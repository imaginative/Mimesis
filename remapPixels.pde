private final String INPUT_FILE_NAME = "/tmp/imageName6.jpg";
private final String INPUT_FILE_NAME2 = "/tmp/imageName4.jpg";
private final int SCALE_X = 800;
private final int SCALE_Y = 0;

PImage from; //image from wich I take the pixels
PImage to;   //image that defines where to place the pixels
PImage canvas;

void settings () {
  from = loadImage(INPUT_FILE_NAME);
  to = loadImage(INPUT_FILE_NAME2);
  from.resize (SCALE_X, SCALE_Y);
  to.resize (from.width, from.height);
  canvas = createImage(from.width, from.height, RGB);

  size (from.width, from.height);
}

void setup() {
  from.loadPixels();
  to.loadPixels();

  //color[] c1 = new color[from.pixels.length];
  //color[] c2 = new color[to.pixels.length];
  int[] p1 = new int[from.pixels.length];
  int[] p2 = new int[to.pixels.length];
  for (int i = 0; i < p1.length; i++) {
    p1[i] = i;
    p2[i] = i;
  }

  doubleSort(from.pixels, p1, 0, from.pixels.length);
  doubleSort(to.pixels, p2, 0, to.pixels.length);

  from.updatePixels();
  to.updatePixels();

  to.loadPixels();
  canvas.loadPixels();
  for (int i = 0; i < canvas.pixels.length; i++) {
    canvas.pixels[p2[i]] = from.pixels[i];
  }
  to.updatePixels();
  canvas.updatePixels();
}

void doubleSort (int[] c, int[] p, int ini, int fim) {
  if (fim - ini == 1) return;
  doubleSort (c, p, ini, (fim-ini)/2 + ini);
  doubleSort (c, p, (fim-ini)/2 + ini, fim);
  intercala (c, p, ini, fim);
}
void intercala (color[] c, int[] p, int ini, int fim) {
  int meio = (fim-ini)/2+ini;
  int j = (fim-ini)/2+ini;
  int i = ini;
  int k = 0;
  color[] v =  new color[fim-ini];
  int[] w = new int[fim-ini];
  PVector vali;
  PVector valj;

  while (i < meio && j < fim) {
    vali = new PVector(red(c[i]), green(c[i]), blue(c[i]));
    valj = new PVector(red(c[j]), green(c[j]), blue(c[j]));
    if (vali.mag() < valj.mag()) {
      v[k] = c[i];
      w[k++] = p[i++];
    } else {
      v[k] = c[j];
      w[k++] = p[j++];
    }
  }
  while (i < meio) {
    v[k] = c[i];
    w[k++] = p[i++];
  }
  while (j < fim) {
    v[k] = c[j];
    w[k++] = p[j++];
  }
  for (int l = ini; l < fim; l++) {
    c[l] = v[l-ini];
    p[l] = w[l-ini];
  }
}

void draw() {
  image(canvas, 0, 0);
}
